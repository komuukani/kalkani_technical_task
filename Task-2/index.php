<?php
$output = '';
if (isset($_POST['send'])) {

    $no = $_POST['number'];
    for ($i = 1; $i <= $no; $i++) {
        $output .= "<br/>";
        $p = 1;
        for ($k = 1; $k <= $no - $i; $k++) {
            $output .= " &nbsp;&nbsp; ";
        }
        for ($j = 1; $j <= $i; $j++) {
            $output .= " $p ";
            $p += 2;
        }
        $ch = 65;
        for ($l = 2; $l <= $i; $l++) {
            $output .= " " . chr($ch) . " ";
            $ch++;
        }
    }
    for ($i = $no - 1; $i > 0; $i--) {
        $output .= "<br/>";
        $p = 1;
        for ($k = 1; $k <= $no - $i; $k++) {
            $output .= " &nbsp;&nbsp; ";
        }
        for ($j = 1; $j <= $i; $j++) {
            $output .= " $p ";
            $p += 2;
        }
        $ch = 65;
        for ($l = 2; $l <= $i; $l++) {
            $output .= " " . chr($ch) . " ";
            $ch++;
        }
    }

}
?>
    <html>
<style>
    body {
        font-family: arial;
        background-color: #dfdfdf;
    }

    input[type=number] {
        width: 100%;
        padding: 15px 20px;
        border: 1px solid #ccc;
        border-radius: 4px;
        font-size: 18px;
        margin: 10px 0;
    }

    .btn {
        background: #000;
        padding: 15px 50px;
        color: #fff;
        border: 1px solid #ccc;
        border-radius: 4px;
        font-size: 18px;
        margin: 10px 0;
    }

    .card {
        width: 50%;
        margin: 50px auto;
        padding: 20px;
        border-radius: 15px;
        box-shadow: 0 0 10px #999;
    }

    label {
        font-weight: bold;
    }

    h1 {
        margin-bottom: 50px;
    }
    .output{
        width: 50%;
        margin: auto;
        background: #000;
        color: #fff;
        padding: 20px;
        line-height: 30px;
        font-size: 20px;
        letter-spacing: 1px;
        border-radius: 15px;
        box-shadow: 0 0 10px #999;
        display: flex;
        justify-content: space-around;
    }
</style>
<body>


<div class="card">
    <form method="post">
        <h1>Print Pattern</h1>
        <label for="fname">Enter Any Number: </label>
        <input type="number" autofocus required id="fname" name="number" placeholder="Your Number...">
        <button class="btn" value="Submit" name="send">Send</button>
    </form>
</div>

    <?php
    if ($output) {
        echo '<div class="output">' . $output . '</div>';
    }
    ?>

</body>
<?php
