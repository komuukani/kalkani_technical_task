<?php
define("DB_SERVER", "localhost");
define("DB_DATABASE", "db_user_info");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");

/*
 * Database connection
 *  */
class DB_Class {
    function __construct() {
        try {
            $odbclink = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_DATABASE, DB_USERNAME, DB_PASSWORD);
            $odbclink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $odbclink->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            if (true === empty($odbclink)) {
                echo "No mysql connection";
                exit;
            }
            $GLOBALS['conn'] = $odbclink;
            return $odbclink;
        } catch (Exception $error) {
            echo $error->getMessage();
            exit;
        }
    }
}