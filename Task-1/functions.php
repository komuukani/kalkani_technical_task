<?php

class Api_Functions
{
    public $db_connection = null;

    public function __construct()
    {
        if ($this->db_connection == null) {
            $db_connection = new DB_Class();
            $this->db_connection = $GLOBALS['conn'];
        }
    }

    public function __call($method, $args)
    {
        $response = array('status' => 'FAILED', 'msg' => 'Oops, invalid method calling!');
        return $response;
    }

    // Add User API
    public function addUser()
    {
        $response = array('status' => '', 'msg' => '');
        $json = file_get_contents('php://input');
        if ($json) {
            $json_array = json_decode($json, true);
            if ((isset($json_array['firstname']) && !empty($json_array['firstname']))
                && (isset($json_array['lastname']) && !empty($json_array['lastname']))
                && (isset($json_array['email']) && !empty($json_array['email']))
                && (isset($json_array['mobileno']) && !empty($json_array['mobileno']))
                && (isset($json_array['address1']) && !empty($json_array['address1']))
                && (isset($json_array['pincode']) && !empty($json_array['pincode']))
                && (isset($json_array['city']) && !empty($json_array['city']))
                && (isset($json_array['state']) && !empty($json_array['state']))) {

                $firstname = $json_array['firstname'];
                $lastname = $json_array['lastname'];
                $email = $json_array['email'];
                $mobileno = $json_array['mobileno'];
                $birthdate = (isset($json_array['birthdate'])) ? $json_array['birthdate'] : "NULL";
                $address1 = $json_array['address1'];
                $address2 = (isset($json_array['address2'])) ? $json_array['address2'] : "NULL";
                $pincode = $json_array['pincode'];
                $city = $json_array['city'];
                $state = $json_array['state'];
                $type = (isset($json_array['type'])) ? $json_array['type'] : "NULL";

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $response['status'] = "FAILED";
                    $response['msg'] = "Please enter valid email";
                } elseif (!preg_match('/^[0-9]{10}+$/', $json_array['mobileno'])) {
                    $response['status'] = "FAILED";
                    $response['msg'] = "Please enter 10 digit number";
                } elseif (strlen($pincode) != 4 && strlen($pincode) != 5 && strlen($pincode) != 6) {
                    $response['status'] = "FAILED";
                    $response['msg'] = "Please enter 4 to 6";
                } else {
                    $featch_records = "SELECT * From tbl_user where email = '$email' || mobileno = '$mobileno'";
                    $records = $this->db_connection->query($featch_records);
                    $records = $records->fetchAll();
                    if (!empty($records)) {
                        $response['status'] = "FAILED";
                        $response['msg'] = "Sorry, email or mobile already exist!!";
                    } else {
                        if (isset($json_array['id']) && !empty($json_array['id'])) {
                            $id = $json_array['id'];
                            $insert_data = 'UPDATE `tbl_user` SET `firstname` = "' . $firstname . '", `lastname` = "' . $lastname . '", `email` = "' . $email . '", `mobileno` = "' . $mobileno . '", `birthdate` = "' . $birthdate . '", `address1` = "' . $address1 . '", `address2` = "' . $address2 . '" , `pincode` = "' . $pincode . '", `city` = "' . $city . '" , `state` ="' . $state . '" , `type` = "' . $type . '" WHERE id= "' . $id . '"';
                        } else {
                            $insert_data = 'INSERT INTO `tbl_user` (`id`, `firstname`, `lastname`, `email`, `mobileno`, `birthdate`, `address1`, `address2`, `pincode`, `city`, `state`, `type`) VALUES (0,"' . $firstname . '","' . $lastname . '","' . $email . '","' . $mobileno . '","' . $birthdate . '","' . $address1 . '","' . $address2 . '","' . $pincode . '","' . $city . '","' . $state . '","' . $type . '") ';
                        }
                        $records = $this->db_connection->query($insert_data);
                        if ($records) {
                            $response['status'] = "SUCCESS";
                            $response['msg'] = "Yeah, User inserted successfully.";
                        } else {
                            $response['status'] = "FAILED";
                            $response['msg'] = "something went wrong!!";
                        }
                    }
                }
            } else {
                $response['status'] = "FAILED";
                $response['msg'] = "Please enter the value";
            }
        }
        return $response;
    }

    // Get All User
    public function getAllUser()
    {
        $response = array('status' => '', 'msg' => '');
        $featch_records = "SELECT * From `tbl_user`";
        $records = $this->db_connection->query($featch_records);
        $records = $records->fetchAll();
        if (empty($records)) {
            $response['status'] = "FAILED";
            $response['data'] = $records;
            $response['msg'] = "Sorry, data not found!";
        } else {
            $response['status'] = "SUCCESS";
            $response['data'] = $records;
            $response['msg'] = "Yeah, Data found";
        }
        return $response;
    }

    // Filter User by City, Age, Firstname, Lastname and Email
    public function filterUser()
    {
        $records = '';
        if ((isset($_POST['city']) && !empty($_POST['city'])) && (isset($_POST['age']) && !empty($_POST['age']))) {
            // Age and city
            $age = $_POST['age'];
            $city = $_POST['city'];
            $featch_records = "SELECT *, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), `birthdate`)), '%Y') + 0 AS age FROM tbl_user WHERE LOWER(`city`) = LOWER('$city') HAVING  age >= $age";
            $records = $this->db_connection->query($featch_records);
            $records = $records->fetchAll();

        } elseif (isset($_POST['city']) && !empty($_POST['city']) && !isset($_POST['age'])) {
            $city = $_POST['city'];
            // Only City
            $featch_records = "SELECT * FROM `tbl_user` WHERE LOWER(`city`) = LOWER('$city')";
            $records = $this->db_connection->query($featch_records);
            $records = $records->fetchAll();

        } elseif ((isset($_POST['firstname']) && !empty($_POST['firstname'])) && (isset($_POST['lastname']) && !empty($_POST['lastname'])) && (isset($_POST['email']) && !empty($_POST['email']))) {
            $email = $_POST['email'];
            $fname = $_POST['firstname'];
            $lname = $_POST['lastname'];
            //First name, Last name, email
            $featch_records = "SELECT * FROM `tbl_user` WHERE LOWER(`firstname`) = LOWER('$fname') || LOWER(lastname) = LOWER('$lname') || LOWER(email) = LOWER('$email')";
            $records = $this->db_connection->query($featch_records);
            $records = $records->fetchAll();

        }
        if (empty($records)) {
            $response['status'] = "FAILED";
            $response['data'] = $records;
            $response['msg'] = "Sorry,data not found!";
        } else {
            $response['status'] = "SUCCESS";
            $response['data'] = $records;
            $response['msg'] = "data found";
        }
        return $response;

    }

}