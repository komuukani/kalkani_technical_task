<?php
include_once 'config.php';
include_once 'functions.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');

 $response = array('status' => 'fail', 'msg' => 'Please enter params');
if (isset($_REQUEST['method_name']) && $_REQUEST['method_name'] != '') {
    $obj_client_functions = new Api_Functions();
    $response = call_user_func(array($obj_client_functions, $_REQUEST['method_name']));
   
}
 echo json_encode($response, JSON_PRETTY_PRINT);

?>

